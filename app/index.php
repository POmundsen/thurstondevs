
<?php include 'views/templates/header.php';?>

    <!-- Jumbotron Header -->
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4 text-light">CREATE YOUR DREAM</h1>
            <p class="lead text-light">Providing quality Earthworks, Drainage & Landscaping in the Bay of Plenty & Waikato Regions.</p>
            <hr class="my-2">
            <p class="header-contact text-light">Contact us now for a Free no obligation quote!</p>
            <a class="btn btn-custom" href="views/contact.php">Get in touch!</a>
        </div>
    </div>
    <!-- End of Jumbotron -->

    <!-- Services Cards -->
    <div class="container text-center text-dark">
      <h2>SERVICES</h2>
      <p>At Thurston Developments we take the time to understand your requirements and we appreciate how valuable your property is. We put our customers at the centre of what we do.</p>
      <hr>
      <div class="row">
        <div class="col-sm-6 col-lg-3">
          <div class="card border-0">
            <a href="views/services.php#drainage"><img class="card-img-top" src="public/img/drain.png" alt="Card image cap"></a>
              <div class="card-body">
                <h5 class="card-title"><a class="text-dark" href="views/services.php#drainage">DRAINAGE</a></h5>
                <p class="card-text">
                We provide comprehensive drain laying services from laying new drains to repairs & maintenance of existing 
                drains, for both residential and commercial clients.</p>
                <hr>
                <a class="btn btn-sm" id="btnServices" href="views/services.php#drainage">FIND OUT MORE</a>    
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card border-0">
            <a href="views/services.php#earthworks"><img class="card-img-top" src="public/img/bulldozer.png" alt="Card image cap"></a>
            <div class="card-body">
              <h5 class="card-title"><a class="text-dark" href="views/services.php#earthworks">EARTHWORKS</a></h5>
              <p class="card-text">
              From Siteworks & Excavation to Land Contouring & Trenching. We offer earthwork and earthmoving services for 
              both commercial and residential needs.
              </p>
              <hr>
              <a class="btn btn-sm" id="btnServices" href="views/services.php#earthworks">FIND OUT MORE</a>    
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card border-0">
          <a href="views/services.php#diggerhire"><img class="card-img-top" src="public/img/excavator-1.png" alt="Card image cap"></a>
            <div class="card-body">
              <h5 class="card-title"><a class="text-dark" href="views/services.php#diggerhire">DIGGER HIRE</a></h5>
              <p class="card-text">
                2018 Vi017 Yanmar Digger & Trailer available for Wet or Dry hire. Get in touch with us to find out more about our great rates.
              </p>
              <hr>
              <a class="btn btn-sm" id="btnServices" href="views/services.php#diggerhire">FIND OUT MORE</a>    
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card border-0">
            <a href="views/services.php#landscaping"><img class="card-img-top" src="public/img/spade.png" alt="Card image cap"></a>
            <div class="card-body">
              <h5 class="card-title"><a class="text-dark" href="views/services.php#landscaping">LANDSCAPING</a></h5>
              <p class="card-text">
                Whether you need metal bought in, a driveway access created or just a whole lot of dirt moved to make a fancy 
                new garden, we can do it.
              </p>
              <hr>
              <a class="btn btn-sm" id="btnServices" href="views/services.php#landscaping">FIND OUT MORE</a>    
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End of Service Cards -->

    <!-- About Blurb -->
    <div class="container text-light bg-dark about-section">
      <div class="row title-about">
        <h2 class="light-heading">Your local</h2>
        <h2 class="custom-heading">DRAINAGE</h2>
        <h2 class="light-heading">SPECIALISTS</h2>
      </div>
      <hr>
      <div class="row about-text">
        <p>Welcome to Thurston Developments Ltd. We’re a locally owned and operated business, servicing the Bay of Plenty and Waikato regions. 
        We provide quality Drainage services, Earthworks, and Landscaping. We aim to provide maximum value for money without jeopardizing quality. 
        Thurston Developments works closely with all of its clients to ensure complete satisfaction. We pride ourselves on meeting and exceeding the 
        expectations of our valued customers.</p>
      </div>
    </div>
    <!-- End of About blurb -->

    <!-- Testimonials area -->
    <div class="container text-center text-dark" id="testimonial-container">
      <!-- <section id="testimonial-link"></section>
      <h2>What our clients are saying...</h2>
      <hr>
      <div class="row justify-content-center">
        <div class="col-md-4" id="testimonial">
          <blockquote>Calvin: Sometimes when I'm talking with others, my words can't keep up with my thoughts. I wonder why we think faster than we speak. Hobbes: Probably so we can think twice. </blockquote>
          <div class="author text-dark">
            <img src="img/dark-name.jpg" alt="image"/>
            <h5>Jason Steve <span>Mount Maunganui</span></h5>
          </div>
        </div>
        <div class="col-md-4" id="testimonial">
          <blockquote>Thank you. before I begin, I'd like everyone to notice that my report is in a professional, clear plastic binder...When a report looks this good, you know it'll get an A. That's a tip kids. Write it down.</blockquote>
          <div class="author text-dark">
            <img src="img/light-name.jpg" alt="image"/>
            <h5>Max Conversion<span>Tauranga</span></h5>
          </div>
        </div>
        <div class="col-md-4" id="testimonial">
          <blockquote>My behaviour is addictive functioning in a disease process of toxic co-dependency. I need holistic healing and wellness before I'll accept any responsibility for my actions.</blockquote>
          <div class="author text-dark">
            <img src="img/dark-name.jpg" alt="image"/>
            <h5>Joe Bloggs<span>JBHOMES, Rotorua</span></h5>
          </div>
        </div> -->
      </div>
    </div>
    <!-- End of Testimonials -->

<?php include 'views/templates/footer.php';?>