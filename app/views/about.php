<?php include 'templates/header.php';?>

    <!-- Start of Content -->
    <div class="container-fluid">
        <div class="row title-about about-section bg-dark text-light py-5">
            <h1 class="light-heading">About</h1>
            <h1 class="custom-heading">US</h1>
        </div>
    </div>
    <div class="container">
        <div class="row about-text">
            <h5 class="pt-3">Welcome to Thurston Developments Ltd.</h5>
            <p>We’re a locally owned business, servicing the Bay of Plenty and Waikato regions. 
            We provide Quality Drainage services, Earthworks, and Landscaping. Our work is carried out by Jordan Thurston, a fully qualified and 
            certified Drainlayer with the New Zealand Plumbing Gasfitting and Drainlaying Board.</p>
        </div>
    </div>
    <div class="container clearfix">
        <div class="row about-img">
            <img id="about-img" src="../public/img/photo_edited.jpg" alt="image">
        </div>
            <div class="row about-text">
                <hr style="width: 100%">
                <p>Jordan has worked extensively in both the commercial and residential drainage
                and earthworks industries for the past 10 years. We aim to provide maximum value for money without jeopardizing quality. 
                Thurston Developments works closely with all its clients to ensure complete satisfaction and we pride ourselves on meeting and exceeding the 
                expectations of our valued customers.</p>
                <hr style="width: 100%">
            </div>
            <div class="row about-text py-3">
                <h5>At Thurston Developments, we are only satisfied when you are!</h5>
            </div>
            <div class="text-center pb-4">
                <a href="contact.php" class="btn btn-custom">Get in touch!</a>
            </div>
    </div>
    <!-- End of Content -->

<?php include 'templates/footer.php';?>