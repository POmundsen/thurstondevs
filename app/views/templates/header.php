<!doctype html>
<html lang="en">
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="google-site-verification" content="LMHuYLEW04ZOcmlIP9LFbMPTPkgxc7a-EMmec_jZb8s"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="description" content="Welcome to Thurston Developments Ltd. We’re a locally owned and operated business, servicing the Bay of Plently and Waikato regions. We provide quality Drainage/Drainlaying services, Earthworks, Digger Hire & Landscaping.">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
    <!-- Link to Css folder -->
    <link href="../../public/css/main.css" rel="stylesheet" type="text/css" >
    <!-- Social Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <title>Thurston Developments | Quality Drainage, Earthworks, Landscaping & Digger Hire in the Bay of Plenty & Waikato regions.</title>
    </head>
    <body>
    <!-- Navigation Bar Area -->
    <nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: #ffffff;">
        <img src="../public/img/logo-long.jpg" class="d-inline-block align-top" alt="Thurston Developments" style="max-width: 350px; height: Auto; width: 75%;">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav ml-auto nav-fill">
                <a class="nav-item nav-link" href="../../index.php">Home</a>
                <a class="nav-item nav-link" href="../views/about.php">About Us</a>
                <a class="nav-item nav-link" href="../views/services.php">Services</a>
                <!-- <a class="nav-item nav-link" href="../public_html/index.php#testimonial-link">Testimonials</a> -->
                <a class="nav-item nav-link" href="../views/contact.php">Contact Us</a>
            </div>
        </div>
    </nav>
    <!-- End of NavBar-->