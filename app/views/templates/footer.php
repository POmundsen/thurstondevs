<!-- Footer -->
<footer class="page-footer font-small bg-dark pt-4 text-light">
        <!-- Footer Links -->
        <div class="container text-center text-md-left">
          <!-- Grid row -->
          <div class="row">
            <!-- Grid column -->
            <div class="col-md-4 mx-auto">
              <!-- Content -->
              <h5 class="mt-2 mb-3">Our Mission Statement</h5>
              <p>The Team at Thurston Developments Ltd are committed to providing Professional, Prompt & Efficient Service without compromising on quality or safety.</p>
            </div>
            <!-- Grid column -->
            <hr class="clearfix w-100 d-md-none">
            <!-- Grid column -->
            <div class="col-md-2 mx-auto">
              <!-- Links -->
              <h5 class="mt-2 mb-3">Useful Links</h5>
              <ul class="list-unstyled text-light">
                <li>
                  <a href="../../index.php">Home</a>
                </li>
                <li>
                  <a href="../views/about.php">About us</a>
                </li>
                <li>
                  <a href="../views/services.php">Services</a>
                </li>
                <li>
                  <a href="../views/contact.php">Contact Us</a>
                </li>
              </ul>
            </div>
            <!-- Grid column -->
            <hr class="clearfix w-100 d-md-none">
            <!-- Grid column -->
            <div class="col-md-3 mx-auto">
              <h5 class="mt-2 mb-3">Contact</h5>
              <ul class="list-unstyled">
                <li>
                  <p>Jordan Thurston</p>
                </li>
                <!-- <li>
                  <p>Email: jthurston.td@gmail.com</p>
                </li> -->
                <li>
                  <p>Phone: 027-945-7997</p>
                </li>
                <li>
                  <p>Facebook: <a href="https://www.facebook.com/thurstondevelopments/">facebook.com/ThurstonDevs</a></p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <hr>
        <div class="text-center py-2">
            <h5 class="mb-1">Contact us for a free no obligation quote!</h5>
        </div>
        <div class="text-center py-2">
            <a href="../views/contact.php" class="btn btn-custom">Get in touch!</a>
        </div>
        <!-- Social buttons -->
        <div class="text-center py-2">
            <a href="https://www.facebook.com/thurstondevelopments" class="fa fa-facebook"></a>
            <a href="https://www.instagram.com/thurston.developments" class="fa fa-instagram"></a>
        </div>
        <!-- End of Social buttons -->
        <div class="footer-copyright text-center">© 2019 Copyright:
          <a href="https://thurstondevelopments.co.nz"> Thurstondevelopments.co.nz</a>
        </div>
    </footer>
    <!-- End of Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
    <script src="../../public/js/contact.js"></script>
</body>
</html>