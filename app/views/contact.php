<?php include 'templates/header.php';?>

<!-- Start of Content -->
<div class="container-fluid">
    <div class="row title-about about-section bg-dark text-light py-5">
        <h1 class="light-heading">Contact</h1>
        <h1 class="custom-heading">US</h1>
    </div>
</div>
<div class="container text-center text-dark pt-3">
        <h5>Call us on 0279457997 or use the form below to get in touch!
        </h5>
    </div>
<div class="container">
    <div class="row">
        <div class="col-xl-8 offset-xl-2 py-3">
            <form id="contact-form" method="post" action="contactform.php" role="form">
                <div class="messages">
                </div>
                <div class="controls">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_name">First Name *</label>
                                <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your first name" required="required" data-error="First name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_lastname">Last Name *</label>
                                <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your last name" required="required" data-error="Last name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email" required="required" data-error="Valid email is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_need">Please specify your need *</label>
                                <select id="form_need" name="need" class="form-control" required="required" data-error="Please specify your need.">
                                    <option value=""></option>
                                    <option value="Request quotation">Request a quote</option>
                                    <option value="Request order status">General Information</option>
                                    <option value="Request copy of an invoice">Request copy of an invoice</option>
                                    <option value="Other">Other</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="form_message">Message *</label>
                                <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please, leave us a message."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-custom" value="Send message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-muted">
                            <strong>*</strong> These fields are required.
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.8 -->
    </div>
    <!-- /.row-->
</div>
<!-- End of Content-->

<?php include 'templates/footer.php';?>