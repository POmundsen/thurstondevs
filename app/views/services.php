<?php include 'templates/header.php';?>

    <!-- Start of Content -->
    <div class="container-fluid">
        <div class="row title-about about-section bg-dark text-light py-5">
            <h1 class="light-heading">Our</h1>
            <h1 class="custom-heading">SERVICES</h1>
        </div>
    </div>

    <div class="container text-center text-dark pt-3">
        <p class="mr-1">At Thurston Developments we take the time to understand your requirements and we appreciate how 
            valuable your property is. We put our customers at the centre of what we do.
        </p>
    </div>
    <hr>
    <div class="container text-dark p-3">
        <section id="drainage"></section>
        <div class="row">
            <div class="col-3 px-0">
                <img class="serv-img-top" src="../public/img/drain.png" alt="Card image cap">
            </div>
            <div class="col-9">
                <div class="row">            
                    <h2>DRAINAGE</h2>
                </div>
                <div class="row mr-1">
                    <p>
                        We are experienced in drainage projects of all shapes & sizes whether it's residential or commercial. 
                        We provide comprehensive drain laying services from laying new drains to repairs & maintenance of existing 
                        drains on all types of properties. We can work with your construction team to install 
                        drainage on new builds or provide you with prompt service to fix any existing drainage issues.
                    </p>
                    <div class="row">
                        <div class="col-12">
                            <ul class="service-ul">
                            <li>Storm Water Management</li>
                            <li>Water Course Piping</li>
                            <li>Manholes</li>
                            <li>Drain Extensions</li>
                            <li>Soak Holes</li>
                            <li>Septic Tanks</li>
                            <li>Retention & Detention Tanks</li>
                            <li>Redirecting Waterflow</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="row service-img-row-drain">
            <div class="col-6 pl-0">
                <img src="../public/img/pic2edit.jpg" alt="image" class="service-img-drain">
            </div>
            <div class="col-6 pl-0">
                <img src="../public/img/pic1edit.jpg" alt="image" class="service-img-drain">
            </div>
        </div>    
    </div>

    <hr>

    <div class="container text-dark p-3">
        <section id="earthworks"></section>
        <div class="row">
            <div class="col-3 px-0">
                <img class="serv-img-top" src="../public/img/bulldozer.png" alt="Card image cap">
            </div>
            <div class="col-9">
                <div class="row">            
                    <h2>EARTHWORKS</h2>
                </div>
                <div class="row mr-1">
                    <p>
                    We are committed to on-time, on-budget, safe and high-quality earthmoving services. 
                    You can trust Thurston Developments for all your earthmoving, earthworks and excavation services. 
                    If need be we have access to larger or smaller machinery and can work with you to suit both your budget 
                    and your project requirements.
                    </p>
                    <ul class="service-ul">
                    <li>Siteworks & Excavation</li>
                    <li>Fill, Metal & Top Soil</li>
                    <li>House Site Preparation</li>
                    <li>Land Contouring</li>
                    <li>Site Clearing</li>
                    <li>Trenching</li>
                    </ul> 
                </div>
            </div>
        </div>
        <div class="row service-img-row">
            <div class="col-12">
                <img src="../public/img/earthworks1.jpg" alt="image" class="service-img">
            </div>
            <!-- <div class="col-12 col-sm-6">
               <img src="../public/img/pic3edit.jpg" alt="image" class="service-img">
            </div> -->
        </div>     
    </div>

    <hr>

    <div class="container text-dark p-3">
        <section id="diggerhire"></section>
        <div class="row">
            <div class="col-3 px-0">
                <img class="serv-img-top" src="../public/img/excavator-1.png" alt="Card image cap">
            </div>
            <div class="col-9">
                <div class="row">            
                    <h2>DIGGER HIRE</h2>
                </div>
                <div class="row mr-1">
                    <h5>2018 Vi017 Yanmar</h5>
                    <p>
                    <b>Dry/Wet Hire Available - Contact us <a href="contact.php">here </a>for our rates.</b>
                    <br>
                    The ViO17 goes almost anywhere and works efficiently in tight, narrow areas. The most compact zero tail 
                    swing mini excavator goes where larger excavators can’t and works easily against walls or buildings. 
                    But its small stature doesn’t sacrifice power. The 13.5 hp diesel engine provides the power you need 
                    for the toughest jobs, with dramatic lifting capacity and bucket digging force that belies its size. 
                    Plus Yanmar’s unique, sturdy variable undercarriage provides flexibility, stability and safety.
                    </p>
                </div>
            </div>
        </div>
        <div class="row service-img-row-hire">
            <div class="col-6">
                <img src="../public/img/hirepicedit.jpg" alt="image" class="service-img">
            </div>
            <div class="col-6">
                <img src="../public/img/digpicedit.jpg" alt="image" class="service-img">
            </div>
        </div>     
    </div>

    <hr>

    <div class="container text-dark p-3">
        <section id="landscaping"></section>
        <div class="row">
            <div class="col-3 px-0">
                <img class="serv-img-top" src="../public/img/spade.png" alt="Card image cap">
            </div>
            <div class="col-9">
                <div class="row">            
                    <h2>LANDSCAPING</h2>
                </div>
                <div class="row mr-1">
                    <p>
                    Whether you're renovating your outdoor area or extending your property we can help to assist with 
                    the initial planning and preparation. We have the right tools for working in tight, compact and 
                    confined spaces. Remove the stress of ground-breaking at the start of your landscaping project where a 
                    section is difficult to access. From the groundbreaking and preparation for any retaining walls to the 
                    levelling and designing of your garden paving or driveway, we have got the job covered.
                    </p>
                </div>
            </div>
        </div>
        <div class="row service-img-row">
            <div class="col-12">
                <img src="../public/img/land1edit.jpg" alt="image" class="service-img">
            </div>
            <!-- <div class="col-12 col-sm-6">
                <img src="../public/img/pic1edit.jpg" alt="image" class="service-img">
            </div> -->
        </div>     
    </div>

    <!-- End of Content -->
    
<?php include 'templates/footer.php';?>